import UIKit

class StartingTimer: UIView {
    
    @IBOutlet weak var startingTimerLabel: UILabel!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var questionLabel: UILabel!
    
    static func instanceFromNib() -> StartingTimer {
        return UINib(nibName: "StartingTimer", bundle: nil).instantiate(withOwner: nil, options: nil).first as? StartingTimer ?? StartingTimer()
    }
    
    func configurationAlert() {
        questionLabel.text = "Are you ready?".localized()
        alertView.roundCornersView(radius: 20)
        self.questionLabel.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 40)
    }
}

extension UIView {
    func roundCornersView (radius: CGFloat = 10) {
        self.layer.cornerRadius = radius
    }
}

