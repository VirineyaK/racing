import UIKit

class GameOverViewController: UIViewController {
    
    var dateOfGame = " "
    var userName = " "
    var userScore = " "
    var soundVolume: Float = 0.3
    private var userResults: ResultsModel?
    
    @IBOutlet weak var gameOverLabel: UILabel!
    @IBOutlet weak var recordsButton: UIButton!
    @IBOutlet weak var mainMenuButton: UIButton!
    @IBOutlet weak var dateOfGameLabel: UILabel!
    @IBOutlet weak var userResultsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SoundManager.shared.playSound(soundName: "gameOver", volume: soundVolume)
        self.createLabel()
        self.createInterfaceDesign()
    }
    
    @IBAction func goToMainMenuButtonPressed(_ sender: UIButton) {
        SoundManager.shared.playSound(soundName: "buttonPressed", volume: soundVolume)
        guard let mainMenu = self.storyboard?.instantiateViewController(withIdentifier: "StartingViewController") as? StartingViewController else {
            return
        }
        mainMenu.soundVolume = soundVolume
        mainMenu.modalPresentationStyle = .fullScreen
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func goToResultsTableButtonPressed(_ sender: UIButton) {
        SoundManager.shared.playSound(soundName: "buttonPressed", volume: soundVolume)
        guard let resultsTable = self.storyboard?.instantiateViewController(withIdentifier: "ResultsViewController") as? ResultsViewController else {
            return
        }
        resultsTable.soundVolume = soundVolume
        resultsTable.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(resultsTable, animated: true)
    }
    
    func createLabel() {
        dateOfGameLabel.text = dateOfGame
        userResultsLabel.text = "\(userName) - \(userScore)."
    }
    
    func createInterfaceDesign() {
        
        recordsButton.setTitle("Records".localized(), for: .normal)
        recordsButton.roundCorners()
        recordsButton.dropShadow(color: .gray, offSet: CGSize(width: 5, height: 5))
        recordsButton.addGradient()
        self.recordsButton.titleLabel?.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 30)
        
        mainMenuButton.setTitle("Menu".localized(), for: .normal)
        mainMenuButton.roundCorners()
        mainMenuButton.dropShadow(color: .gray, offSet: CGSize(width: 5, height: 5))
        mainMenuButton.addGradient()
        self.mainMenuButton.titleLabel?.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 30)
        
        self.gameOverLabel.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 45)
        gameOverLabel.text = "Game_over".localized() + "!"
        
        self.dateOfGameLabel.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 30)
        self.gameOverLabel.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 45)
        self.userResultsLabel.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 30)
    }
}
