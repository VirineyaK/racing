import UIKit
import CoreMotion


class GameViewController: UIViewController {
    
    var motionManager = CMMotionManager()
    
    private let sportsCarView = UIImageView()
    private let obstacleImageView = UIImageView()
    private let bonusForScoringImageView = UIImageView()
    private let explosionImageView = UIImageView()
    private let treeImageArray: [UIImage?] = [UIImage(named: "treeOneImage"), UIImage(named: "treeTwoImage"), UIImage(named: "treeThreeImage"), UIImage(named: "treeFourImage"), UIImage(named: "treeFiveImage")]
    
    private var startingCountdownTimer = Timer()
    private var roadMarkingsTimer = Timer()
    private var movementOfTreesOnRightTimer = Timer()
    private var movementOfTreesOnLeftTimer = Timer()
    private var obstaclesMovementTimer = Timer()
    private var bonusMovementTimer = Timer()
    private var timerForCheckOfCarCrushWithObstacle = Timer()
    private var scoringTimer = Timer()
    
    
    private let dateOfGame = Date()
    private var additionalHeight = CGFloat()
    
    private var valueWithDurationAnimation = 4.5
    private var level = 1
    private var userScore = 0
    
    private var userResults: ResultsModel?
    private var userSettings: SettingsModel?
    private var userName = ""
    
    var soundVolume: Float = 0.3
    
    @IBOutlet weak var roadView: UIView!
    @IBOutlet weak var leftRoadsideView: UIView!
    @IBOutlet weak var rightRoadsideView: UIView!
    @IBOutlet weak var obstacleView: UIView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        motionManager.startAccelerometerUpdates()
        self.moveCarAccelerometer()
        
        motionManager.gyroUpdateInterval = 10/60
        motionManager.startGyroUpdates()
        
        
        self.userSettings = SettingsManager.shared.getSettings()
        userName = userSettings?.userName ?? "Игрок"
        
        self.userResults = ResultsManager.shared.getResults()
        self.userResults?.userName = userName
        self.userResults?.dateOfGame = convertDateToString(dateOfGame)
        
        self.createTimerView()
        
        self.createCar()
        self.createRoadMarkings()
        self.createObstacleOnRoad()
        self.createBonusOnRoad()
        self.scoreUser()
        self.monitorCarCrushWithObstacle()
        
        additionalHeight = self.view.frame.size.height / 8
        scoreLabel.text = "Score".localized() + ": 0"
        levelLabel.text = "Level".localized() + ": 1"
        
    }
    
    // MARK: - Moving object control
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if event?.subtype == .motionShake {
            self.createCarJump()
            print ("SHAKE")
        }
    }
    
    @IBAction func moveCarToLeftSwipe ( sender: UISwipeGestureRecognizer){
        SoundManager.shared.playSound(soundName: "swipe", volume: soundVolume)
        UIView.animate(withDuration: 0.3) {
            UIImageView.animate(withDuration: 0.1) {
                if self.sportsCarView.frame.origin.x > self.leftRoadsideView.frame.size.width + 14 {
                    self.sportsCarView.frame.origin.x -= self.view.frame.size.width / 3
                } else { }
            }
        }
    }
    
    @IBAction func moveCarToRightSwipe ( sender: UISwipeGestureRecognizer){
        SoundManager.shared.playSound(soundName: "swipe", volume: soundVolume)
        UIImageView.animate(withDuration: 0.3) {
            if self.sportsCarView.frame.origin.x < self.rightRoadsideView.frame.origin.x - self.sportsCarView.frame.size.width - 14 {
                self.sportsCarView.frame.origin.x += self.view.frame.size.width / 3
            } else { }
        }
    }
    
    @IBAction func tap (sender: UITapGestureRecognizer){
        createCarJump()
    }
    
    private func moveCarAccelerometer() {
        let timer = Timer.scheduledTimer(withTimeInterval: 10/60, repeats: true) { (_) in
            if (self.motionManager.accelerometerData?.acceleration.x ?? 0) > 0.0 {
                UIImageView.animate(withDuration: 0.5) {
                    if self.sportsCarView.frame.origin.x < self.rightRoadsideView.frame.origin.x - self.sportsCarView.frame.size.width - 14 {
                        self.sportsCarView.frame.origin.x += 15
                    } else { }
                }
            } else if (self.motionManager.accelerometerData?.acceleration.x ?? 0) < 0.0 {
                UIImageView.animate(withDuration: 0.5) {
                    if self.sportsCarView.frame.origin.x > self.leftRoadsideView.frame.size.width + 14 {
                        self.sportsCarView.frame.origin.x -= 15
                    } else { }
                }
            }
        }
        timer.fire()
    }
    
    // MARK: - Сreation of objects game
    private func createCar() {
        sportsCarView.image = UIImage(named: userSettings?.car ?? "sportsCarYellowImage")
        sportsCarView.contentMode = .scaleAspectFit
        let yPosition = self.view.frame.size.height * 0.77
        let xPosition = self.view.frame.size.width * 0.55
        let width = self.view.frame.size.width / 4.5
        let height = self.view.frame.size.height / 8
        sportsCarView.frame = CGRect(x: xPosition, y: yPosition, width: width, height: height)
        self.view.addSubview(sportsCarView)
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(moveCarToLeftSwipe(sender:)))
        leftSwipe.direction = .left
        self.view.addGestureRecognizer(leftSwipe)
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(moveCarToRightSwipe(sender:)))
        rightSwipe.direction = .right
        self.view.addGestureRecognizer(rightSwipe)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tap(sender:)))
        self.view.addGestureRecognizer(tap)
    }
    
    private func createCarJump() {
        self.timerForCheckOfCarCrushWithObstacle.invalidate()
        
        SoundManager.shared.playSound(soundName: "jump", volume: soundVolume)
        UIView.animate(withDuration: 0.9, delay: 0, options: .curveLinear, animations: {
            self.sportsCarView.transform = CGAffineTransform(scaleX:  1.4, y: 1.4)
            
        }) { (_) in
            UIView.animate(withDuration: 0.8, delay: 0, options: .curveLinear, animations: {
                self.sportsCarView.transform = CGAffineTransform(scaleX:  1, y:  1)
                
            }) { (_) in
                self.monitorCarCrushWithObstacle()
            }}
    }
    
    private func createRoadMarkings() {
        let width = self.roadView.frame.size.width / 18
        
        roadMarkingsTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (_) in
            let roadMarkings = UIView() // был интервал 1.2
            
            roadMarkings.frame = CGRect(x: self.roadView.frame.size.width / 2 - (width/2), y: 0 - self.additionalHeight, width: width, height: self.additionalHeight)
            
            roadMarkings.backgroundColor = .white
            self.roadView.addSubview(roadMarkings)
            
            UIView.animate(withDuration: self.valueWithDurationAnimation, delay: 0.0, options: .curveLinear, animations: {
                roadMarkings.frame.origin.y = self.roadView.frame.size.height + self.additionalHeight
            }) { (_) in
                roadMarkings.removeFromSuperview()
            }
        })
        roadMarkingsTimer.fire()
    }
    
    private func movementOfTreesOnRightRoadside() {
        let height = self.view.frame.size.height / 11
        
        movementOfTreesOnRightTimer = Timer.scheduledTimer(withTimeInterval: 2.4, repeats: true, block: { (_) in
            let treeImageView = UIImageView()
            treeImageView.frame = CGRect(x: self.rightRoadsideView.frame.origin.x, y: 0 - self.additionalHeight, width: self.rightRoadsideView.frame.size.width, height: height)
            treeImageView.image = self.treeImageArray.randomElement()!
            treeImageView.contentMode = .scaleToFill
            self.leftRoadsideView.addSubview(treeImageView)
            
            UIImageView.animate(withDuration: self.valueWithDurationAnimation, delay: 0.0, options: .curveLinear, animations: {
                treeImageView.frame.origin.y = self.view.frame.size.height + self.additionalHeight
            }) { (_) in
                treeImageView.removeFromSuperview()
            }
        })
        self.movementOfTreesOnRightTimer.fire()
    }
    
    private func movementOfTreesOnLeftRoadside() {
        let height = self.view.frame.size.height / 11
        movementOfTreesOnLeftTimer = Timer.scheduledTimer(withTimeInterval: 2.4, repeats: true, block: { (_) in
            let treeImageView = UIImageView()
            treeImageView.frame = CGRect(x: self.leftRoadsideView.frame.origin.x, y: 0 - (self.additionalHeight * 2), width: self.leftRoadsideView.frame.size.width, height: height)
            treeImageView.image = self.treeImageArray.randomElement()!
            treeImageView.contentMode = .scaleToFill
            self.leftRoadsideView.addSubview(treeImageView)
            
            UIImageView.animate(withDuration: self.valueWithDurationAnimation + 0.6, delay: 0.0, options: .curveLinear, animations: {
                treeImageView.frame.origin.y = self.view.frame.size.height + self.additionalHeight
            }) { (_) in
                treeImageView.removeFromSuperview()
            }
        })
        self.movementOfTreesOnLeftTimer.fire()
    }
    
    private func createObstacleOnRoad() {
        let width = self.view.frame.size.width / 4.5
        obstaclesMovementTimer = Timer.scheduledTimer(withTimeInterval: 6.5 , repeats: true, block: { (_) in
            self.obstacleImageView.frame.size = CGSize(width: width, height: self.additionalHeight)
            self.obstacleImageView.frame.origin.y = -1 * self.additionalHeight
            let arrayNumberForOriginX: [CGFloat] = [(self.leftRoadsideView.frame.size.width + 14), (self.rightRoadsideView.frame.origin.x - self.sportsCarView.frame.size.width - 14)]
            self.obstacleImageView.frame.origin.x = arrayNumberForOriginX.randomElement()!
            self.obstacleImageView.image = UIImage(named: self.userSettings?.typeOfObstacle ?? "obstacleStoneImage")
            self.obstacleImageView.contentMode = .scaleAspectFit
            self.obstacleView.addSubview(self.obstacleImageView)
            
            UIImageView.animate(withDuration: self.valueWithDurationAnimation, delay: 0.0, options: .curveLinear, animations: {
                self.obstacleImageView.frame.origin.y = self.view.frame.size.height + self.additionalHeight
            })
            { (_) in
                self.obstacleImageView.removeFromSuperview()
            }
        })
    }
    
    // MARK: - Creating a countdown timer before starting a game
    
    private func createTimerView() {
        let timerView = StartingTimer.instanceFromNib()
        timerView.configurationAlert()
        view.isUserInteractionEnabled = false
        timerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        
        self.view.addSubview(timerView)
        
        timerView.startingTimerLabel.tag = 3
        startingCountdownTimer = Timer.scheduledTimer(withTimeInterval: 1.2, repeats: true, block: { (_) in
            if timerView.startingTimerLabel.tag != 1 {
                timerView.startingTimerLabel.tag -= 1
                timerView.startingTimerLabel.text = "\(timerView.startingTimerLabel.tag)"
            } else {
                timerView.removeFromSuperview()
                self.startingCountdownTimer.invalidate()
                self.movementOfTreesOnLeftRoadside()
                self.movementOfTreesOnRightRoadside()
                self.view.isUserInteractionEnabled = true
            }
        })
    }
    
    
    // MARK: - Сreation of functions for counting game results
    
    private func createBonusOnRoad() {
        let sizeBonus = self.view.frame.size.width / 8
        var timeInterval = 6.5
        if (self.obstacleImageView.frame).intersects(self.bonusForScoringImageView.layer.presentation()?.frame ?? CGRect(x: 0, y: 0, width: 0, height: 0)) {
            
            timeInterval += 1
        }
        bonusMovementTimer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: true, block: { (_) in
            self.bonusForScoringImageView.frame.size = CGSize(width: sizeBonus, height: sizeBonus)
            self.bonusForScoringImageView.frame.origin.y = -1 * sizeBonus
            let arrayNumberForOriginX: [CGFloat] = [(self.leftRoadsideView.frame.size.width + (sizeBonus / 2)), (self.rightRoadsideView.frame.origin.x - self.sportsCarView.frame.size.width)]
            self.bonusForScoringImageView.frame.origin.x = arrayNumberForOriginX.randomElement()!
            self.bonusForScoringImageView.image = UIImage(named: "starForScorImage")
            self.bonusForScoringImageView.contentMode = .scaleAspectFit
            self.view.addSubview(self.bonusForScoringImageView)
            
            
            UIImageView.animate(withDuration: self.valueWithDurationAnimation - 0.5, delay: 0.0, options: .curveLinear, animations: {
                self.bonusForScoringImageView.frame.origin.y = self.view.frame.size.height + sizeBonus
            })
            { (_) in
                self.scoreLabel.text = "Score".localized() + ": \(self.userScore)"
                if self.userScore % 5 == 0 && self.userScore < 16 {
                    self.level += 1
                    self.levelLabel.text = "Level".localized() + ": \(self.level)"
                    self.valueWithDurationAnimation -= 0.5
                } else if self.userScore % 10 == 0 && self.userScore > 16 {
                    self.level += 1
                    self.levelLabel.text = "Level".localized() + ": \(self.level)"
                    self.valueWithDurationAnimation -= 0.25
                }
                self.bonusForScoringImageView.removeFromSuperview()
            }
        })
    }
    
    private func scoreUser() {
        scoringTimer = Timer.scheduledTimer(withTimeInterval: 0.05, repeats: true, block: { (_) in
            if self.sportsCarView.frame.intersects(self.bonusForScoringImageView.layer.presentation()?.frame ?? CGRect(x: 0, y: 0, width: 0, height: 0)) {
                SoundManager.shared.playSound(soundName: "bonusSound", volume: self.soundVolume)
                self.bonusForScoringImageView.removeFromSuperview()
                self.userScore += 1
                self.userResults?.score = self.userScore
            }
        })
    }
    
    // MARK: - Сreation an explosion and ending the game
    
    private func createExplosion() {
        explosionImageView.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        explosionImageView.center = sportsCarView.center
        SoundManager.shared.playSound(soundName: "obstacle", volume: soundVolume)
        explosionImageView.image = UIImage(named: "boomImage")
        view.addSubview(explosionImageView)
        UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveLinear, animations: {
            self.explosionImageView.frame.size.width += 50
            self.explosionImageView.frame.size.height += 50
        }) { (true) in
            self.explosionImageView.removeFromSuperview()
        }
    }
    
    private func monitorCarCrushWithObstacle() {
        
        timerForCheckOfCarCrushWithObstacle = Timer.scheduledTimer(withTimeInterval: 0.05, repeats: true, block: { (_) in
            if self.sportsCarView.frame.intersects(self.obstacleImageView.layer.presentation()?.frame ?? CGRect(x: 0, y: 0, width: 0, height: 0)) {
                
                
                self.stopMovementAllAnimation()
                self.createExplosion()
                self.gameOver()
                self.obstacleImageView.removeFromSuperview()
                self.sportsCarView.removeFromSuperview()
                
                self.timerForCheckOfCarCrushWithObstacle.invalidate()
                self.scoringTimer.invalidate()
                self.obstaclesMovementTimer.invalidate()
                self.roadMarkingsTimer.invalidate()
                self.movementOfTreesOnRightTimer.invalidate()
                self.movementOfTreesOnLeftTimer.invalidate()
                self.bonusMovementTimer.invalidate()
                
                self.motionManager.stopAccelerometerUpdates()
                if let userResults = self.userResults {
                    ResultsManager.shared.setResults(userResults)
                }
            }
        })
    }
    
    private func stopMovementAllAnimation() {
        let when = DispatchTime.now() + 0.2
        DispatchQueue.main.asyncAfter(deadline: when) {
            let pausedTime: CFTimeInterval = self.view.layer.convertTime(CACurrentMediaTime(), from: nil)
            self.view.layer.speed = 0.0
            self.view.layer.timeOffset = pausedTime
        }
    }
    
    private func gameOver() {
        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when) {
            guard let gameOverViewController = self.storyboard?.instantiateViewController(withIdentifier: "GameOverViewController") as? GameOverViewController else {
                return
            }
            gameOverViewController.modalPresentationStyle = .fullScreen
            gameOverViewController.dateOfGame = "\(self.convertDateToString(self.dateOfGame))"
            gameOverViewController.userName = "\(self.userName)"
            gameOverViewController.userScore = "\(self.userScore)"
            gameOverViewController.soundVolume = self.soundVolume
            self.navigationController?.pushViewController(gameOverViewController, animated: true)
        }
    }
    
    private func convertDateToString(_ date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy HH:mm"
        return formatter.string(from: date)
    }
}

