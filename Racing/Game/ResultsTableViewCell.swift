import UIKit

class ResultsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var resultsNumerationLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var gameDateLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var rewardImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        rewardImageView.image = nil
    }
    
    func configure (with userResults: ResultsModel) {
        userNameLabel.text = userResults.userName
        gameDateLabel.text = userResults.dateOfGame
        scoreLabel.text = String(userResults.score)
    }
    
    func showImage (with image: UIImage) {
        self.rewardImageView.image = image
    }
}
