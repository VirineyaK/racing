import UIKit
import CoreMotion
import AVKit
import AVFoundation

class StartingViewController: UIViewController {
    
    private var settings: SettingsModel?
    var soundVolume: Float = 0.3
    
    @IBOutlet weak var startGameButton: UIButton!
    @IBOutlet weak var showRecordsButton: UIButton!
    @IBOutlet weak var showSettingsButton: UIButton!
    @IBOutlet weak var nameUserLabel: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var soundButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SoundManager.shared.playSound(soundName: "startingSound", volume: soundVolume)
        self.createInterfaceDesign()
        self.addParallaxToView(view: backgroundImageView, magnitude: 100)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        SoundManager.shared.playSound(soundName: "startingSound", volume: soundVolume)
        self.settings = SettingsManager.shared.getSettings()
        if settings != nil {
            nameUserLabel.text = "Hello".localized() + ", \(settings?.userName ?? "User")!"
        }
    }
    
    @IBAction func startGameButtonPressed(_ sender: UIButton) {
        SoundManager.shared.stopSound(soundName: "startingSound")
        SoundManager.shared.playSound(soundName: "buttonPressed", volume: soundVolume)
        guard let gameController = self.storyboard?.instantiateViewController(withIdentifier: "GameViewController") as? GameViewController else {
            return
        }
        gameController.soundVolume = soundVolume
        gameController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(gameController, animated: true)
    }
    
    @IBAction func showRecordsButtonPressed(_ sender: UIButton) {
        SoundManager.shared.stopSound(soundName: "startingSound")
        SoundManager.shared.playSound(soundName: "buttonPressed", volume: soundVolume)
        guard let resultsController = self.storyboard?.instantiateViewController(withIdentifier: "ResultsViewController") as? ResultsViewController else {
            return
        }
        resultsController.soundVolume = soundVolume
        resultsController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(resultsController, animated: true)
    }
    
    @IBAction func showSettingsButtonPressed(_ sender: UIButton) {
        SoundManager.shared.stopSound(soundName: "startingSound")
        SoundManager.shared.playSound(soundName: "buttonPressed", volume: soundVolume)
        
        if let settingsController = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController {
            settingsController.soundVolume = soundVolume
            settingsController.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(settingsController, animated: true)
        }
        
    }
    @IBAction func turnSoundOffButtonPressed(_ sender: UIButton) {
        if soundButton.isSelected == true {
            soundButton.isSelected = false
                SoundManager.shared.playSound(soundName: "startingSound", volume: self.soundVolume)
                self.soundVolume = 0.3
        } else {
            soundButton.isSelected = true
            SoundManager.shared.stopSound(soundName: "startingSound")
            soundVolume = 0
        }
        if let settings = self.settings {
            SettingsManager.shared.setSettings(settings)
        }
        
    }
    
    func addParallaxToView(view: UIImageView, magnitude: Float) {
        let horizontal = UIInterpolatingMotionEffect(keyPath: "layer.transform.translation.x", type: .tiltAlongHorizontalAxis)
        horizontal.minimumRelativeValue = -magnitude
        horizontal.maximumRelativeValue = magnitude
        
        let vertical = UIInterpolatingMotionEffect(keyPath: "layer.transform.translation.y", type: .tiltAlongVerticalAxis)
        vertical.minimumRelativeValue = -magnitude
        vertical.maximumRelativeValue = magnitude
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        view.addMotionEffect(group)
    }
    
    
    
    
    func createInterfaceDesign() {
        showRecordsButton.setTitle("Records".localized(), for: .normal)
        showSettingsButton.setTitle("Settings".localized(), for: .normal)
        
        startGameButton.roundCorners()
        startGameButton.dropShadow(color: .gray, offSet: CGSize(width: 5, height: 5))
        startGameButton.addGradient()
        showRecordsButton.roundCorners()
        showRecordsButton.dropShadow(color: .gray, offSet: CGSize(width: 5, height: 5))
        showRecordsButton.addGradient()
        showSettingsButton.roundCorners()
        showSettingsButton.dropShadow(color: .gray, offSet: CGSize(width: 5, height: 5))
        showSettingsButton.addGradient()
        
        let textForStartGameButtonOne = "Start".localized()
        let textForStartGameButtonTwo = "Game".localized()
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let secondAttributes = [NSAttributedString.Key.foregroundColor: UIColor.yellow]
        let attributedString = NSMutableAttributedString(string: textForStartGameButtonOne, attributes: attributes)
        let secondattributedString = NSAttributedString(string: textForStartGameButtonTwo, attributes: secondAttributes)
        attributedString.append(secondattributedString)
        startGameButton.setAttributedTitle(attributedString, for: .normal)
        self.nameUserLabel.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 35)
        self.startGameButton.titleLabel?.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 30)
        self.showRecordsButton.titleLabel?.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 30)
        self.showSettingsButton.titleLabel?.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 30)
    }
}

extension UIButton {
    
    func roundCorners(radius: CGFloat = 10) {
        self.layer.cornerRadius = radius
    }
    
    func dropShadow(color: UIColor, opacity: Float = 2.0, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 10).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func addGradient() {
        let gradient = CAGradientLayer()
        
        gradient.colors = [UIColor.black.cgColor, UIColor.yellow.cgColor]
        
        gradient.startPoint = CGPoint(x: 0.4, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.cornerRadius = 10
        gradient.frame = self.bounds
        self.layer.addSublayer(gradient)
    }
}



