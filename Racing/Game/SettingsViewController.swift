import UIKit

class SettingsViewController: UIViewController {
    
    private var userSettings: SettingsModel?
    
    var soundVolume: Float = 0.3
    
    @IBOutlet weak var createUserNameTextField: UITextField!
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var obstacleImageView: UIImageView!
    @IBOutlet weak var colorCarLabel: UILabel!
    @IBOutlet weak var typeOfObstacleLabel: UILabel!
    @IBOutlet weak var saveSettingsButton: UIButton!
    @IBOutlet weak var saveNameButton: UIButton!
    @IBOutlet weak var redCarButton: UIButton!
    @IBOutlet weak var greenCarButton: UIButton!
    @IBOutlet weak var blueCarButton: UIButton!
    @IBOutlet weak var yellowCarButton: UIButton!
    @IBOutlet weak var stoneObstacleButton: UIButton!
    @IBOutlet weak var ufoObstacleButton: UIButton!
    @IBOutlet weak var carObstacleButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userSettings = SettingsManager.shared.getSettings()
        
        carImageView.image = UIImage(named: userSettings?.car ?? "sportsCarYellowImage")
        obstacleImageView.image = UIImage(named: userSettings?.typeOfObstacle ?? "obstacleStoneImage")
        createUserNameTextField.placeholder = userSettings?.userName
        self.createInterfaceDesign()
        
        self.registerForKeyboardNotifications()
        self.hideKeyboard()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if let userSettings = self.userSettings {
            SettingsManager.shared.setSettings(userSettings)
        }
    }
    
    @IBAction func tapDetectedHidingKeyboard(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @IBAction func saveUserNameButtonPressed(_ sender: UIButton) {
        SoundManager.shared.playSound(soundName: "buttonPressed", volume: soundVolume)
        self.userSettings?.userName = createUserNameTextField.text
    }
    
    @IBAction func chooseRedCarButtonPressed(_ sender: UIButton) {
        SoundManager.shared.playSound(soundName: "buttonPressed", volume: soundVolume)
        self.userSettings?.car = "sportsCarRedImage"
        carImageView.image = UIImage(named: "sportsCarRedImage")
    }
    
    @IBAction func chooseGreenCarButtonPressed(_ sender: UIButton) {
        SoundManager.shared.playSound(soundName: "buttonPressed", volume: soundVolume)
        self.userSettings?.car = "sportsCarGreenImage"
        carImageView.image = UIImage(named: "sportsCarGreenImage")
    }
    
    @IBAction func chooseBlueCarButtonPressed(_ sender: UIButton) {
        SoundManager.shared.playSound(soundName: "buttonPressed", volume: soundVolume)
        self.userSettings?.car = "sportsCarBlueImage"
        carImageView.image = UIImage(named: "sportsCarBlueImage")
    }
    
    @IBAction func chooseYellowCarButtonPressed(_ sender: UIButton) {
        SoundManager.shared.playSound(soundName: "buttonPressed", volume: soundVolume)
        self.userSettings?.car = "sportsCarYellowImage"
        carImageView.image = UIImage(named: "sportsCarYellowImage")
    }
    
    @IBAction func chooseStonesObstacleButtonPressed(_ sender: UIButton) {
        SoundManager.shared.playSound(soundName: "buttonPressed", volume: soundVolume)
        self.userSettings?.typeOfObstacle = "obstacleStoneImage"
        obstacleImageView.image = UIImage(named: "obstacleStoneImage")
    }
    
    @IBAction func chooseUFOButtonPressed(_ sender: UIButton) {
        SoundManager.shared.playSound(soundName: "buttonPressed", volume: soundVolume)
        self.userSettings?.typeOfObstacle = "flyingSaucerImage"
        obstacleImageView.image = UIImage(named: "flyingSaucerImage")
    }
    
    @IBAction func chooseCarsObstaclButtonPressed(_ sender: UIButton) {
        SoundManager.shared.playSound(soundName: "buttonPressed", volume: soundVolume)
        self.userSettings?.typeOfObstacle = "obstacleGrayCarImage"
        obstacleImageView.image = UIImage(named: "obstacleGrayCarImage")
    }
    
    @IBAction func goToMainMenuButtonPressed(_ sender: UIButton) {
        SoundManager.shared.playSound(soundName: "buttonPressed", volume: soundVolume)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(_ notification: NSNotification) {
        let userInfo = notification.userInfo!
        let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
    }
    
    private func hideKeyboard() {
        let recognizerTapHidingKeyboard = UITapGestureRecognizer(target: self, action: #selector(tapDetectedHidingKeyboard(_:)))
        self.view.addGestureRecognizer(recognizerTapHidingKeyboard)
    }
    
    private func createInterfaceDesign() {
        colorCarLabel.text = "Car_color".localized()
        self.colorCarLabel.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 30)
        typeOfObstacleLabel.text = "Obstacle".localized()
        self.typeOfObstacleLabel.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 30)
        
        saveSettingsButton.setTitle("Save_settings".localized(), for: .normal)
        saveSettingsButton.roundCorners()
        saveSettingsButton.dropShadow(color: .gray, offSet: CGSize(width: 5, height: 5))
        saveSettingsButton.addGradient()
        self.saveSettingsButton.titleLabel?.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 28)
        
        saveNameButton.setTitle("Save_name".localized(), for: .normal)
        saveNameButton.roundCorners()
        saveNameButton.dropShadow(color: .gray, offSet: CGSize(width: 5, height: 5))
        saveNameButton.addGradient()
        self.saveNameButton.titleLabel?.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 25)
        
        redCarButton.setTitle("Red".localized(), for: .normal)
        redCarButton.roundCorners()
        redCarButton.dropShadow(color: .gray, offSet: CGSize(width: 5, height: 5))
        self.redCarButton.titleLabel?.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 20)
        
        greenCarButton.setTitle("Green".localized(), for: .normal)
        greenCarButton.roundCorners()
        greenCarButton.dropShadow(color: .gray, offSet: CGSize(width: 5, height: 5))
        self.greenCarButton.titleLabel?.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 20)
        
        blueCarButton.setTitle("Blue".localized(), for: .normal)
        blueCarButton.roundCorners()
        blueCarButton.dropShadow(color: .gray, offSet: CGSize(width: 5, height: 5))
        self.blueCarButton.titleLabel?.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 20)
        
        yellowCarButton.setTitle("Yellow".localized(), for: .normal)
        yellowCarButton.roundCorners()
        yellowCarButton.dropShadow(color: .gray, offSet: CGSize(width: 5, height: 5))
        self.yellowCarButton.titleLabel?.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 20)
        
        stoneObstacleButton.setTitle("Stone".localized() , for: .normal)
        stoneObstacleButton.roundCorners()
        stoneObstacleButton.dropShadow(color: .gray, offSet: CGSize(width: 5, height: 5))
        self.stoneObstacleButton.titleLabel?.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 20)
        
        ufoObstacleButton.setTitle("UFO".localized(), for: .normal)
        ufoObstacleButton.roundCorners()
        ufoObstacleButton.dropShadow(color: .gray, offSet: CGSize(width: 5, height: 5))
        self.ufoObstacleButton.titleLabel?.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 20)
        
        carObstacleButton.setTitle("Car".localized(), for: .normal)
        carObstacleButton.roundCorners()
        carObstacleButton.dropShadow(color: .gray, offSet: CGSize(width: 5, height: 5))
        self.carObstacleButton.titleLabel?.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 20)
    }
}

extension Notification.Name {
    static let keyboardWillShowNotification = Notification.Name("keyboardWillShowNotification")
    static let keyboardWillHideNotification = Notification.Name("keyboardWillHideNotification")
}

extension SettingsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.userSettings?.userName = createUserNameTextField.text
        return true
    }
}
