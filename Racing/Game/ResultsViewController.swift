import UIKit

class ResultsViewController: UIViewController {
    
    var userResults = [ResultsModel]()
    var soundVolume = Float()
    
    @IBOutlet weak var highscoresLabel: UILabel!
    @IBOutlet weak var resultsTableView: UITableView!
    @IBOutlet weak var mainMenuButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userResults = ResultsManager.shared.getResultsArray() ?? []
        resultsTableView.reloadData()
        self.createInterfaceDesign()
        self.removeResultsFromArray()
    }
    
    
    @IBAction func goToMainMenuButtonPressed(_ sender: UIButton) {
        SoundManager.shared.playSound(soundName: "buttonPressed", volume: soundVolume)
        
        guard let mainMenu = self.storyboard?.instantiateViewController(withIdentifier: "StartingViewController") as? StartingViewController else {
            return
        }
        mainMenu.modalPresentationStyle = .fullScreen
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    func removeResultsFromArray() {
        if userResults.count > 20 {
            userResults.removeSubrange(20..<userResults.count)
        }
        ResultsManager.shared.setResultsArray(userResults)
    }
    
    func createInterfaceDesign() {
        highscoresLabel.text = "Highscores".localized()
        self.highscoresLabel.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 40)
        
        mainMenuButton.setTitle("Menu".localized(), for: .normal)
        mainMenuButton.roundCorners()
        mainMenuButton.dropShadow(color: .gray, offSet: CGSize(width: 5, height: 5))
        mainMenuButton.addGradient()
        self.mainMenuButton.titleLabel?.font = UIFont(name: "v_TypographyofCoop-Heavy", size: 30)
    }
}

extension ResultsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = resultsTableView.dequeueReusableCell(withIdentifier: "ResultsTableViewCell", for: indexPath) as? ResultsTableViewCell else {
            return ResultsTableViewCell()
        }
        
        cell.configure(with: userResults[indexPath.row])
        cell.resultsNumerationLabel.text = String(indexPath.row + 1)
        
        switch indexPath.row {
        case 0:
            cell.showImage(with: UIImage(named: "goldImage")!)
        case 1:
            cell.showImage(with: UIImage(named: "silverImage")!)
        case 2:
            cell.showImage(with: UIImage(named: "bronzeImage")!)
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
}
