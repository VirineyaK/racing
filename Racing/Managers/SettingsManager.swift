
import Foundation

class SettingsManager {
    
    static let shared = SettingsManager()
    private init () {}
    
    private let defaultsSettings = SettingsModel(userName: "Игрок", car: "sportsCarYellowImage", typeOfObstacle: "obstacleStoneImage", sound: true)
    private let keySettings = "key"
    
    func getSettings() -> SettingsModel {
        if let settings = UserDefaults.standard.value(SettingsModel.self, forKey: keySettings) {
            return settings
        }
        return defaultsSettings
    }
    
    func setSettings(_ settings: SettingsModel) {
        UserDefaults.standard.set(encodable: settings, forKey: keySettings)
    }
}

extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}
