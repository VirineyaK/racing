import Foundation
import UIKit

class ResultsManager {
    static let shared = ResultsManager()
    private init () {}
    
    //    var resultsArray = [ResultsModel]()
    let defaultsResults = ResultsModel(score: 0, userName: "name", dateOfGame: " ")
    private let keyResults = "keyResults"
    
    func getResults() -> ResultsModel {
        if let resultsArray = self.getResultsArray() {
            return resultsArray.last ?? defaultsResults
        }
        return defaultsResults
    }
    
    func getResultsArray() -> [ResultsModel]? {
        if var resultsArray = UserDefaults.standard.value([ResultsModel].self, forKey: keyResults) {
            return resultsArray
        }
        return []
    }
    
    func setResults(_ results: ResultsModel) {
        var resultsArray = self.getResultsArray()
        resultsArray?.append(results)
        resultsArray?.sort(by: {$0.score > $1.score})
        self.setResultsArray(resultsArray ?? [])
//        print(resultsArray?.count)
        
    }
    
    func deleteResults(index: Int) {
        if var resultsArray = self.getResultsArray() {
            resultsArray.remove(at: index)
            self.setResultsArray(resultsArray ?? [])
        }
    }
    
    func setResultsArray(_ results: [ResultsModel]) {
        UserDefaults.standard.set(encodable: results, forKey: keyResults)
        
    }
    
    //       func setResultsArray(_ results: [ResultsModel]?) {
    //            UserDefaults.standard.set(encodable: results, forKey: keyResults)
    //        var arrayAppend = ResultsManager.shared.resultsArray
    //        print(arrayAppend.count)
    //        }
    //    }
}

