
import Foundation
import UIKit
import AVKit
import AVFoundation


class SoundManager {
    
    static let shared = SoundManager()
    private init () {}
    
    var audioPlayer = AVAudioPlayer()
    
    func createAudoiPlayer(soundName: String) {
        guard let file = Bundle.main.path(forResource: soundName, ofType: "mp3") else {
            return
        }
        let url = URL(fileURLWithPath: file)
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            try AVAudioSession.sharedInstance().setActive(true)
            try audioPlayer = AVAudioPlayer(contentsOf: url)
            audioPlayer.prepareToPlay()
        } catch let error {
            print(error)
        }
    }
    
    func playSound(soundName: String, volume: Float) {
        
        self.createAudoiPlayer(soundName: soundName)
        audioPlayer.play()
        audioPlayer.volume = volume
    }
    
    func stopSound(soundName: String) {
        self.createAudoiPlayer(soundName: soundName)
        audioPlayer.stop()
    }
}
