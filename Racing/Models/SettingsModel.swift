

import Foundation

class SettingsModel: NSObject, Codable {
    var userName: String?
    var car: String?
    var typeOfObstacle: String?
    var sound: Bool?
    
    init(userName: String, car: String, typeOfObstacle: String, sound: Bool) {
        self.userName = userName
        self.car = car
        self.typeOfObstacle = typeOfObstacle
        self.sound = sound
    }
    
    public enum CodingKeys: String, CodingKey {
        case userName, car, typeObstacle, sound
    }
    
    public override init() {
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.userName, forKey: .userName)
        try container.encode(self.car, forKey: .car)
        try container.encode(self.typeOfObstacle, forKey: .typeObstacle)
        try container.encode(self.sound, forKey: .sound)
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.userName = try (container.decodeIfPresent(String.self, forKey: .userName) ?? "Игрок")
        self.car = try (container.decodeIfPresent(String.self, forKey: .car) ?? "colorCar")
        self.typeOfObstacle = try (container.decodeIfPresent(String.self, forKey: .typeObstacle) ?? "typeObstacle")
        self.sound = try (container.decodeIfPresent(Bool.self, forKey: .sound) ?? true)
    }
}
