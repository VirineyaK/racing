import UIKit
import Foundation

class ResultsModel: NSObject, Codable {
    var score: Int
    var userName: String?
    var dateOfGame: String?
    
    init(score: Int, userName: String, dateOfGame: String) {
        self.score = score
        self.userName = userName
        self.dateOfGame = dateOfGame
    }
    
    public enum CodingKeys: String, CodingKey {
        case score, userName, dateOfGame
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.score, forKey: .score)
        try container.encode(self.userName, forKey: .userName)
        try container.encode(self.dateOfGame, forKey: .dateOfGame)
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.score = try (container.decodeIfPresent(Int.self, forKey: .score) ?? 0)
        self.userName = try (container.decodeIfPresent(String.self, forKey: .userName) ?? "name")
        self.dateOfGame = try (container.decodeIfPresent(String.self, forKey: .dateOfGame) ?? "date")
    }
}
